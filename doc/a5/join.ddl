﻿-- Project Name : M4P
-- Date/Time    : 2018/03/02 15:18:46
-- Author       : shimazaki
-- RDBMS Type   : MySQL
-- Application  : A5:SQL Mk-2

-- BANリスト
drop table if exists ban_list cascade;

create table ban_list (
  organization_id SMALLINT not null auto_increment comment '団体ID'
  , ban_user_id VARCHAR(12) not null comment 'BANユーザーID'
  , ban_reason VARCHAR(128) comment 'BAN理由'
  , regist_date DATETIME default CURRENT_TIMESTAMP comment '登録日時'
  , regist_user VARCHAR(12) comment '登録ユーザー'
  , update_date DATETIME default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新日時'
  , update_user VARCHAR(12) comment '更新ユーザー'
  , constraint ban_list_PKC primary key (organization_id,ban_user_id)
) comment 'BANリスト' ;

-- インフォメーション
drop table if exists information cascade;

create table information (
  id INT not null auto_increment comment 'id'
  , title VARCHAR(32) not null comment 'タイトル'
  , contents VARCHAR(1024) not null comment '本文'
  , target_organization_id SMALLINT default null comment '対象団体:nullなら全員向け'
  , regist_date DATETIME default CURRENT_TIMESTAMP comment '登録日時'
  , regist_user VARCHAR(12) comment '登録ユーザー'
  , update_date DATETIME default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新日時'
  , update_user VARCHAR(12) comment '更新ユーザー'
  , constraint information_PKC primary key (id)
) comment 'インフォメーション' ;

-- お気に入り
drop table if exists bookmark cascade;

create table bookmark (
  guest_user_id VARCHAR(12) not null comment 'ゲストユーザーID'
  , host_user_id VARCHAR(12) not null comment 'ホストユーザーID'
  , regist_date DATETIME default CURRENT_TIMESTAMP comment '登録日時'
  , regist_user VARCHAR(12) comment '登録ユーザー'
  , update_date DATETIME default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新日時'
  , update_user VARCHAR(12) comment '更新ユーザー'
  , constraint bookmark_PKC primary key (guest_user_id,host_user_id)
) comment 'お気に入り' ;

-- ログイン履歴
drop table if exists login_history cascade;

create table login_history (
  id INT not null auto_increment comment 'ID'
  , user_id VARCHAR(12) not null comment 'ユーザーID'
  , user_agent_info VARCHAR(64) not null comment 'ユーザーエージェント情報'
  , ip_address VARCHAR(32) not null comment 'IPアドレス'
  , regist_date DATETIME default CURRENT_TIMESTAMP comment '登録日時'
  , regist_user VARCHAR(12) comment '登録ユーザー'
  , update_date DATETIME default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新日時'
  , update_user VARCHAR(12) comment '更新ユーザー'
  , constraint login_history_PKC primary key (id)
) comment 'ログイン履歴' ;

-- ゲームタイトルマスタ
drop table if exists game_title_master cascade;

create table game_title_master (
  title_cd SMALLINT not null AUTO_INCREMENT comment 'タイトルコード'
  , display_title VARCHAR(64) not null comment 'タイトル名'
  , display_order SMALLINT not null comment '表示順'
  , regist_date DATETIME default CURRENT_TIMESTAMP comment '登録日時'
  , regist_user VARCHAR(12) comment '登録ユーザー'
  , update_date DATETIME default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新日時'
  , update_user VARCHAR(12) comment '更新ユーザー'
  , constraint game_title_master_PKC primary key (title_cd)
) comment 'ゲームタイトルマスタ' ;

-- 入金管理
drop table if exists payment_management cascade;

create table payment_management (
  id INT not null auto_increment comment 'ID'
  , organization_id SMALLINT not null comment '団体ID'
  , deposit_amount INT not null comment '入金額'
  , cutoff_date DATETIME not null comment '締め日'
  , payment_date DATETIME not null comment '振込日'
  , payment_status TINYINT default 0 not null comment '振込状況:0:未入金、1:入金済み、2:エラー'
  , regist_date DATETIME default CURRENT_TIMESTAMP comment '登録日時'
  , regist_user VARCHAR(12) comment '登録ユーザー'
  , update_date DATETIME default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新日時'
  , update_user VARCHAR(12) comment '更新ユーザー'
  , constraint payment_management_PKC primary key (id)
) comment '入金管理' ;

-- 団体
drop table if exists organization cascade;

create table organization (
  id SMALLINT not null auto_increment comment 'ID'
  , name VARCHAR(64) not null comment '団体名'
  , financial_institution_cd SMALLINT not null comment '金融機関コード'
  , account_type TINYINT not null comment '口座種別'
  , branch_code VARCHAR(32) not null comment '支店番号'
  , bank_account VARCHAR(32) not null comment '口座番号'
  , regist_date DATETIME default CURRENT_TIMESTAMP comment '登録日時'
  , regist_user VARCHAR(12) comment '登録ユーザー'
  , update_date DATETIME default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新日時'
  , update_user VARCHAR(12) comment '更新ユーザー'
  , constraint organization_PKC primary key (id)
) comment '団体' ;

-- ゲスト
drop table if exists guest cascade;

create table guest (
  id VARCHAR(12) not null comment 'ID'
  , title_cd SMALLINT comment 'デフォルトタイトルコード'
  , tickets_num INT default 0 not null comment '保有チケット数'
  , out_guest_id VARCHAR(128) comment '外部ゲストID:決済処理で外部が認識するためのID'
  , regist_date DATETIME default CURRENT_TIMESTAMP comment '登録日時'
  , regist_user VARCHAR(12) comment '登録ユーザー'
  , update_date DATETIME default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新日時'
  , update_user VARCHAR(12) comment '更新ユーザー'
  , constraint guest_PKC primary key (id)
) comment 'ゲスト' ;

-- ユーザー
drop table if exists user cascade;

create table user (
  id VARCHAR(12) not null comment 'ID:IDはユーザが4~12文字の中で決める。
大文字は使用不可。使用できる記号は「_ (アンダーバー)」のみ。'
  , display_name VARCHAR(20) not null comment '表示名'
  , status TINYINT not null comment '状況:0:無効、1:有効'
  , type TINYINT default 0 not null comment 'タイプ:0:ゲスト、1:ホスト、2:団体管理'
  , mailaddress VARCHAR(30) not null comment 'メールアドレス'
  , password_hash VARCHAR(64) not null comment 'パスワードハッシュ'
  , previous_password_hash VARCHAR(64) not null comment '前回パスワードハッシュ'
  , sso_id VARCHAR(128) comment 'SSOID'
  , sso_type VARCHAR(32) comment 'SSOタイプ'
  , regist_date DATETIME default CURRENT_TIMESTAMP comment '登録日時'
  , regist_user VARCHAR(12) comment '登録ユーザー'
  , update_date DATETIME default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新日時'
  , update_user VARCHAR(12) comment '更新ユーザー'
  , constraint user_PKC primary key (id)
) comment 'ユーザー' ;

-- 支払方法マスタ
drop table if exists payment_method_master cascade;

create table payment_method_master (
  payment_method_cd SMALLINT not null auto_increment comment '支払方法コード'
  , payment_method VARCHAR(32) default "" not null comment '支払方法:カード名とかを入れる'
  , regist_date DATETIME default CURRENT_TIMESTAMP comment '登録日時'
  , regist_user VARCHAR(12) comment '登録ユーザー'
  , update_date DATETIME default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新日時'
  , update_user VARCHAR(12) comment '更新ユーザー'
  , constraint payment_method_master_PKC primary key (payment_method_cd)
) comment '支払方法マスタ' ;

-- 支払履歴
drop table if exists payment_history cascade;

create table payment_history (
  id INT not null auto_increment comment 'ID'
  , user_id VARCHAR(12) not null comment 'ユーザーID'
  , method_cd TINYINT not null comment '支払方法コード'
  , payment_number SMALLINT not null comment '購入チケット数'
  , out_history_id VARCHAR(128) not null comment '外部支払い履歴'
  , regist_date DATETIME default CURRENT_TIMESTAMP comment '登録日時'
  , regist_user VARCHAR(12) comment '登録ユーザー'
  , update_date DATETIME default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新日時'
  , update_user VARCHAR(12) comment '更新ユーザー'
  , constraint payment_history_PKC primary key (id)
) comment '支払履歴' ;

-- ホストマッチ
drop table if exists host_match cascade;

create table host_match (
  id INT not null auto_increment comment 'ID'
  , match_title VARCHAR(30) not null comment '予定名'
  , host_user_id VARCHAR(12) not null comment 'ホストユーザーID'
  , recruitment_numbers SMALLINT not null comment '募集人数'
  , apply_method_id TINYINT not null comment '入札方法コード'
  , request_tickets SMALLINT not null comment '必要チケット'
  , status TINYINT not null comment '状況'
  , start_time DATETIME not null comment '開始日時'
  , end_time DATETIME not null comment '終了日時'
  , tile_cd SMALLINT not null comment 'タイトルコード'
  , comment VARCHAR(256) comment 'コメント'
  , regist_date DATETIME default CURRENT_TIMESTAMP comment '登録日時'
  , regist_user VARCHAR(12) comment '登録ユーザー'
  , update_date DATETIME default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新日時'
  , update_user VARCHAR(12) comment '更新ユーザー'
  , constraint host_match_PKC primary key (id)
) comment 'ホストマッチ' ;

-- ゲストマッチ
drop table if exists guest_match cascade;

create table guest_match (
  id INT not null auto_increment comment 'ID'
  , guest_user_id VARCHAR(12) not null comment 'ゲストユーザーID'
  , host_user_id VARCHAR(12) not null comment 'ホストユーザーID'
  , host_match_id INT not null comment 'ホストマッチID'
  , consumption_ticket SMALLINT not null comment '消費チケット'
  , status TINYINT default 0 not null comment '状況:0:応募済み確定前、1:当選・落札、2:抽選漏れ、3:入札失敗、9:その他落選'
  , assess_cd TINYINT default 0 not null comment '評価コード:ホストへの評価、初期値（未評価）0'
  , comment VARCHAR(256) comment 'コメント:評価コメント'
  , regist_date DATETIME default CURRENT_TIMESTAMP comment '登録日時'
  , regist_user VARCHAR(12) comment '登録ユーザー'
  , update_date DATETIME default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新日時'
  , update_user VARCHAR(12) comment '更新ユーザー'
  , constraint guest_match_PKC primary key (id)
) comment 'ゲストマッチ' ;

-- ホスト
drop table if exists host cascade;

create table host (
  id VARCHAR(12) not null comment 'ID'
  , title_cd SMALLINT default 0 not null comment 'デフォルトタイトルコード'
  , default_comment VARCHAR(128) not null comment 'デフォルトコメント'
  , organization_id SMALLINT not null comment '団体ID'
  , authority_level TINYINT default 0 not null comment '権限レベル:0:一般ホスト、9:団体管理'
  , profeal_comment VARCHAR(512) not null comment 'プロフィールコメント'
  , regist_date DATETIME default CURRENT_TIMESTAMP comment '登録日時'
  , regist_user VARCHAR(12) comment '登録ユーザー'
  , update_date DATETIME default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '更新日時'
  , update_user VARCHAR(12) comment '更新ユーザー'
  , constraint host_PKC primary key (id)
) comment 'ホスト' ;

