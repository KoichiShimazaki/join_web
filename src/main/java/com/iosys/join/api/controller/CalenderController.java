package com.iosys.join.api.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * カレンダー処理API用のコントローラー
 * @author shimazaki
 *
 */
@RestController
@RequestMapping("api/icon")
public class CalenderController {

	/**
	 * 月間カレンダー情報取得
	 * @param userId ユーザーID
	 * @param yyyymm 年月
	 * 
	 * ユーザーIDは必須
	 * 年月の指定が無い場合は、現在月を指定された扱いとする
	 * */
	public void getByMonth(@RequestBody String body) {
		
	}
	
	/**
	 * 日次カレンダー情報取得
	 * @param userId ユーザーID
	 * @param yyyymmdd 年月日
	 * 
	 * ユーザーIDは必須
	 * 年月日の指定が無い場合は、現在日を指定された扱いとする
	 * */
	public void getByDay(@RequestBody String body) {
		
	}
}
