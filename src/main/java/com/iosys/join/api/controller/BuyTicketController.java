package com.iosys.join.api.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/ticket")
public class BuyTicketController {
	
	/**
	 * チケット購入
	 * @param クレジットカード決済用トークン
	 * @param アカウントID
	 * @param 支払い方法
	 * @param 購入チケット数
	 * 
	 * ※JOIN内の処理に入る前の処理について
	 * クレジットカード決済用トークンは外部のクレジットカード決済用apiを使用して作成する
	 * 
	 * 1.クレジットカード決済用トークンを使用して決済処理
	 * 2.支払履歴テーブルにアカウントID、支払方法、購入チケット数を登録
	 * 3.処理結果を返却する
	 * 
	 * */
	@RequestMapping(value="buy",method=RequestMethod.POST)
	public void buy() {
		
	}
}
