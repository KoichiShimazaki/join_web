package com.iosys.join.api.controller;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.iosys.join.entity.Guest;
import com.iosys.join.entity.User;

/**
 * 認証処理API用のコントローラー
 * @author shimazaki
 *
 */
@RestController
@RequestMapping("api/auth")
public class AuthenticationController {

	@Autowired
	private SqlSession sqlSession;
	  
	/**
	 * ログイン処理
	 * @param エージェント情報
	 * @param IPアドレス
	 * @param アカウントID
	 * @param アカウントパスワード
	 * 
	 * 1.HTTPRequestからエージェント情報、IPアドレス、アカウントID、アカウントパスワードを取得
	 * 2.アカウントIDとアカウントパスワードで認証処理
	 * 3.ログイン履歴テーブルにアカウントID、エージェント情報、IPアドレスを追加
	 * 4.アカウントIDでアカウントの種類を確認
	 * 5.アカウント種類に合致するログイン後TOP画面に遷移
	 * 
	 * */
	@RequestMapping(value="login")
	String login() {
		System.out.println(new Object(){}.getClass().getEnclosingMethod().getName());
		//http://localhost:8080/auth/login
		System.out.println("aaaa");
		
		String userId = "1";
		User aa = (User)sqlSession.selectOne("com.iosys.join.map.UserMapper.selectByPrimaryKey", userId);
		
		//テストコード
		String id = "join";
		Guest guest = (Guest)sqlSession.selectOne("com.iosys.join.map.GuestMapper.selectByPrimaryKey",id);
		System.out.println(guest.getId());
		//テストコード
		
		return "Hello World!" + aa.getDisplayName();
	}
	
	/**
	 * ログアウト処理
	 * @param アカウントID
	 * 
	 * 1.HTTPRequestからアカウントIDを取得
	 * 2.アカウントIDで認証処理
	 * 3.ログイン前TOP画面に遷移
	 * 
	 * */
	@RequestMapping(value="logout",method=RequestMethod.POST)
	public void logout() {
		
	}
	/**
	 * リマインダー（パスワード忘れ対応処理）
	 * @param アカウントID
	 * 
	 * 1.HTTPRequestからアカウントIDを取得
	 * 2.アカウントIDでアカウント情報を検索
	 * 3.アカウント情報のメールアドレスにパスワード変更画面のURLをメールで送信
	 * 4.処理結果を返却
	 * 
	 * */
	@RequestMapping(value="reminder",method=RequestMethod.POST)
	public int reminder() {
		return 0;
	}
}
