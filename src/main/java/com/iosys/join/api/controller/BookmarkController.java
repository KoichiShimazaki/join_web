package com.iosys.join.api.controller;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.iosys.join.object.JoinResponseObject;
import com.iosys.join.web.service.BookmarkService;

/**
 * お気に入り関連処理API用のコントローラー
 * @author shimazaki
 *
 */
@RestController
@RequestMapping("api/bookmark")
public class BookmarkController {

	@Autowired
	private BookmarkService bookmarkService;
	private JoinResponseObject res = new JoinResponseObject();
	//テストコード
	private Logger logger = Logger.getLogger(new Object(){}.getClass().getName());
	/**
	 * お気に入り登録処理
	 * @param gestId ユーザーID
	 * @param hostId 対象ユーザーID
	 * 
	 * @return ブックマーク登録の処理結果コードをjsonで返却
	 * 
	 * */
	@PostMapping//(produces = MediaType.APPLICATION_JSON_VALUE)
	public JoinResponseObject bookmark(@RequestParam(name="gestId") String gestId,
			@RequestParam(name="hostId") String hostId) {
		
		//テストコード
		logger.info("処理開始 : " + new Object(){}.getClass().getName());
		
		//1.使用不可能な文字列などが含まれてないかチェック 今の実装はとりあえず空じゃないか見るだけの適当なやつ
		if(!gestId.equals(null) && !hostId.equals(null)) {
			//2.お気に入り登録処理
			int result = bookmarkService.insertBookmark(gestId, hostId);
			
			//テストコード
			System.out.println("処理結果 : " + result);
			//テストコード
			
			res.setResultCd(result);
		}
		//テストコード
		logger.info("処理終了 : " + new Object(){}.getClass().getName());
		//jsonを返す処理 オブジェクトを返却するとspring boot がよしなにやってくれるらしい
		return res;
	}
}
