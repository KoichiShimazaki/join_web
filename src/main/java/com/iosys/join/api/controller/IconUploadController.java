package com.iosys.join.api.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * アイコン登録用API用のコントローラー
 * @author shimazaki
 *
 */
@RestController
@RequestMapping("api/icon")
public class IconUploadController {

	/**
	 * アイコンのアップロード処理（ユーザー）
	 * @param userId ユーザーID
	 * 
	 * 1.アイコンをtemp領域にアップロードする。
	 * */
	@PostMapping("user")
	public void user(@RequestBody String body) {
	}

	/**
	 * アイコンのアップロード処理（団体）
	 * @param orgId 団体ID
	 * 
	 * 1.アイコンをtemp領域にアップロードする。
	 * */
	@PostMapping("org")
	public void org(@RequestBody String body) {
	}
}
