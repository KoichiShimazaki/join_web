package com.iosys.join.api.controller;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * マッチ情報処理API用のコントローラー
 * @author shimazaki
 *
 */
@RestController
@RequestMapping("icon")
public class MatchController {

	/**
	 * マッチ情報取得処理
	 * @param matchId マッチID
	 * 
	 * マッチ情報をID指定で取得する
	 * 
	 * */
	public void get(@RequestBody String body) {
		
	}

	/**
	 * マッチ情報更新処理
	 * @param matchId マッチID
	 * 
	 * マッチ情報をID指定で更新する
	 * 
	 * */
	public void update(@RequestBody String body) {
		
	}
	
	/**
	 * マッチ情報削除処理
	 * @param matchId マッチID
	 * 
	 * マッチ情報をID指定で削除する
	 * 
	 * */
	public void delete(@RequestBody String body) {
		
	}

	/**
	 * マッチ情報登録処理
	 * @param matchId マッチID
	 * 
	 * マッチ情報をID指定で削除する
	 * 
	 * */
	public void regist(@RequestBody String body) {
		
	}
}
