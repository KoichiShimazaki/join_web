package com.iosys.join.object;

public class JoinResponseObject {

	private int resultCd;
	
	public int getResultCd() {
		return resultCd;
	}
	public void setResultCd(int resultCd) {
		this.resultCd = resultCd;
	}
}
