package com.iosys.join.dao;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iosys.join.entity.Guest;

@Repository
public class GuestDaoImpl implements GuestDao{
	
	@Autowired
	private SqlSession sqlSession;
	
	/**
	 * ゲスト検索（1件）
	 * @param userId 検索対象のユーザーID
	 * 
	 * @return ゲストの検索結果を返却　検索結果が0件ならnullを返却する
	 * */
	@Override
	public Guest selectOne(String userId) throws Exception{
		Guest guest = (Guest)sqlSession.selectOne("com.iosys.join.map.GuestMapper.selectByPrimaryKey",userId);
		return guest;
	}
}
