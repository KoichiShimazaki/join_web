package com.iosys.join.dao;

import java.util.Date;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.iosys.join.entity.Bookmark;

@Repository
public class BookmarkDaoImpl implements BookmarkDao{

	@Autowired
	private SqlSession sqlSession;
	//テストコード
	private Logger logger = Logger.getLogger(new Object(){}.getClass().getName());
	
	/**
	 * ブックマークテーブル挿入処理
	 * @param guestId 主キー1
	 * @param hostId 主キー2
	 * 
	 * */
	@Transactional
	@Override
	public void insert(String guestId,String hostId) throws Exception {
		
		//テストコード
		logger.info("処理開始 : " + new Object(){}.getClass().getName());
		
		Bookmark bm = new Bookmark();
		Date date = new Date();
		
		//挿入データを設定
		bm.setGuestUserId(guestId);
		bm.setHostUserId(hostId);
		bm.setRegistDate(date);
		bm.setRegistUser(guestId);
		bm.setUpdateDate(null);
		bm.setUpdateUser(null);
		
		//インサート処理実行
		sqlSession.insert("com.iosys.join.map.BookmarkMapper.insert",bm);
		
		//テストコード
		logger.info("処理終了 : " + new Object(){}.getClass().getName());
	}
	
	/**
	 * ブックマーク登録済みホスト検索（1件）
	 * @param guestId 主キー1
	 * @param hostId 主キー2
	 * 
	 * @return ブックマークの検索結果を返却　検索結果が0件ならnullを返却する
	 * */
	@Override
	public Bookmark selectOne(String guestId,String hostId) throws Exception {
		
		//テストコード
		logger.info("処理開始 : " + new Object(){}.getClass().getName());
				
		Bookmark selectConditions = new Bookmark();

		//検索条件設定
		selectConditions.setGuestUserId(guestId);
		selectConditions.setHostUserId(hostId);
		
		//検索処理実行
		Bookmark bookmark = (Bookmark)sqlSession.selectOne("com.iosys.join.map.BookmarkMapper.selectByPrimaryKey",selectConditions);
		
		//テストコード
		logger.info("処理終了 : " + new Object(){}.getClass().getName());
		
		return bookmark;
	}
	
}
