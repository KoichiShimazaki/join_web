package com.iosys.join.dao;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iosys.join.entity.User;

@Repository
public class UserDaoImpl implements UserDao {

	@Autowired
	private SqlSession sqlSession;
	//テストコード
	private Logger logger = Logger.getLogger(new Object(){}.getClass().getName());
	
	@Override
	public User select(String userId) throws Exception {
		
		//テストコード
		logger.info("処理開始 : " + new Object(){}.getClass().getName());
				
		User user = (User)sqlSession.selectOne("com.iosys.join.map.UserMapper.selectByPrimaryKey",userId);
		
		//テストコード
		logger.info("処理終了 : " + new Object(){}.getClass().getName());
				
		return user;
	}
}
