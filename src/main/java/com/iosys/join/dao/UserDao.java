package com.iosys.join.dao;

import com.iosys.join.entity.User;

public interface UserDao {

	public User select(String userId) throws Exception;
}
