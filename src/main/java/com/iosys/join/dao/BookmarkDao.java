package com.iosys.join.dao;

import com.iosys.join.entity.Bookmark;

public interface BookmarkDao {

	public void insert(String gestId,String hostId) throws Exception;
	public Bookmark selectOne(String guestId,String hostId) throws Exception;
}
