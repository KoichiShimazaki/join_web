package com.iosys.join.dao;

import com.iosys.join.entity.Guest;

public interface GuestDao {

	public Guest selectOne(String userId) throws Exception;
}
