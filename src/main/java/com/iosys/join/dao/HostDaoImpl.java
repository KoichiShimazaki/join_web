package com.iosys.join.dao;

import org.apache.ibatis.session.SqlSession;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.iosys.join.entity.Host;

@Repository
public class HostDaoImpl implements HostDao{

	@Autowired
	private SqlSession sqlSession;
	//テストコード
	private Logger logger = Logger.getLogger(new Object(){}.getClass().getName());
	/**
	 * ホスト検索（1件）
	 * @param userId 検索対象のユーザーID
	 * 
	 * @return ホストの検索結果を返却　検索結果が0件ならnullを返却する
	 * */
	@Override
	public Host selectOne(String userId) throws Exception{
		
		//テストコード
		logger.info("処理開始 : " + new Object(){}.getClass().getName());
				
		Host host = (Host)sqlSession.selectOne("com.iosys.join.map.HostMapper.selectByPrimaryKey", userId);
		
		//テストコード
		logger.info("処理終了 : " + new Object(){}.getClass().getName());
				
		return host;
	}
}
