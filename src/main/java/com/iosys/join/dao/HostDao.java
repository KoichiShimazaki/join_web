package com.iosys.join.dao;

import com.iosys.join.entity.Host;

public interface HostDao {

	public Host selectOne(String userId) throws Exception;
}
