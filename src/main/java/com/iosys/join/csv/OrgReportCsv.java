package com.iosys.join.csv;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

/**
 * 団体管理画面のレポート用CSV
 * @author shimazaki
 *
 */
@JsonPropertyOrder({"団体ID","ID", "名前", "マッチID","ゲストID","ゲスト表示名","チケット数","登録日時"})
public class OrgReportCsv {
    @JsonProperty("ID")
    private Long id;
    @JsonProperty("名前")
    private String name;
    @JsonProperty("概要")
    private String desc;
    @JsonProperty("更新日時")
    private Date modified;

    public OrgReportCsv() {}

    public OrgReportCsv(Long id, String name, String desc, Date modified) {
        this.id = id;
        this.name = name;
        this.desc = desc;
        this.modified = modified;
    }

    // Getter/Setter
}