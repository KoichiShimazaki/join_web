package com.iosys.join.entity;

import java.util.Date;

public class Host {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column host.id
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column host.title_cd
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private Short titleCd;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column host.default_comment
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String defaultComment;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column host.organization_id
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private Short organizationId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column host.authority_level
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private Byte authorityLevel;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column host.profeal_comment
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String profealComment;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column host.regist_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private Date registDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column host.regist_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String registUser;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column host.update_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private Date updateDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column host.update_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String updateUser;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column host.id
	 * @return  the value of host.id
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column host.id
	 * @param id  the value for host.id
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column host.title_cd
	 * @return  the value of host.title_cd
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public Short getTitleCd() {
		return titleCd;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column host.title_cd
	 * @param titleCd  the value for host.title_cd
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setTitleCd(Short titleCd) {
		this.titleCd = titleCd;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column host.default_comment
	 * @return  the value of host.default_comment
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getDefaultComment() {
		return defaultComment;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column host.default_comment
	 * @param defaultComment  the value for host.default_comment
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setDefaultComment(String defaultComment) {
		this.defaultComment = defaultComment == null ? null : defaultComment.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column host.organization_id
	 * @return  the value of host.organization_id
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public Short getOrganizationId() {
		return organizationId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column host.organization_id
	 * @param organizationId  the value for host.organization_id
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setOrganizationId(Short organizationId) {
		this.organizationId = organizationId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column host.authority_level
	 * @return  the value of host.authority_level
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public Byte getAuthorityLevel() {
		return authorityLevel;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column host.authority_level
	 * @param authorityLevel  the value for host.authority_level
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setAuthorityLevel(Byte authorityLevel) {
		this.authorityLevel = authorityLevel;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column host.profeal_comment
	 * @return  the value of host.profeal_comment
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getProfealComment() {
		return profealComment;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column host.profeal_comment
	 * @param profealComment  the value for host.profeal_comment
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setProfealComment(String profealComment) {
		this.profealComment = profealComment == null ? null : profealComment.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column host.regist_date
	 * @return  the value of host.regist_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public Date getRegistDate() {
		return registDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column host.regist_date
	 * @param registDate  the value for host.regist_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setRegistDate(Date registDate) {
		this.registDate = registDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column host.regist_user
	 * @return  the value of host.regist_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getRegistUser() {
		return registUser;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column host.regist_user
	 * @param registUser  the value for host.regist_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setRegistUser(String registUser) {
		this.registUser = registUser == null ? null : registUser.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column host.update_date
	 * @return  the value of host.update_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column host.update_date
	 * @param updateDate  the value for host.update_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column host.update_user
	 * @return  the value of host.update_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getUpdateUser() {
		return updateUser;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column host.update_user
	 * @param updateUser  the value for host.update_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser == null ? null : updateUser.trim();
	}
}