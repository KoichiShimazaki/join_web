package com.iosys.join.entity;

import java.util.Date;

public class User {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column user.id
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column user.display_name
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String displayName;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column user.status
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private Byte status;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column user.type
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private Byte type;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column user.mailaddress
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String mailaddress;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column user.password_hash
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String passwordHash;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column user.previous_password_hash
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String previousPasswordHash;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column user.sso_id
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String ssoId;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column user.sso_type
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String ssoType;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column user.regist_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private Date registDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column user.regist_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String registUser;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column user.update_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private Date updateDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column user.update_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String updateUser;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column user.id
	 * @return  the value of user.id
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column user.id
	 * @param id  the value for user.id
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setId(String id) {
		this.id = id == null ? null : id.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column user.display_name
	 * @return  the value of user.display_name
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getDisplayName() {
		return displayName;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column user.display_name
	 * @param displayName  the value for user.display_name
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName == null ? null : displayName.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column user.status
	 * @return  the value of user.status
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public Byte getStatus() {
		return status;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column user.status
	 * @param status  the value for user.status
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setStatus(Byte status) {
		this.status = status;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column user.type
	 * @return  the value of user.type
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public Byte getType() {
		return type;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column user.type
	 * @param type  the value for user.type
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setType(Byte type) {
		this.type = type;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column user.mailaddress
	 * @return  the value of user.mailaddress
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getMailaddress() {
		return mailaddress;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column user.mailaddress
	 * @param mailaddress  the value for user.mailaddress
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setMailaddress(String mailaddress) {
		this.mailaddress = mailaddress == null ? null : mailaddress.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column user.password_hash
	 * @return  the value of user.password_hash
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getPasswordHash() {
		return passwordHash;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column user.password_hash
	 * @param passwordHash  the value for user.password_hash
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setPasswordHash(String passwordHash) {
		this.passwordHash = passwordHash == null ? null : passwordHash.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column user.previous_password_hash
	 * @return  the value of user.previous_password_hash
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getPreviousPasswordHash() {
		return previousPasswordHash;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column user.previous_password_hash
	 * @param previousPasswordHash  the value for user.previous_password_hash
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setPreviousPasswordHash(String previousPasswordHash) {
		this.previousPasswordHash = previousPasswordHash == null ? null : previousPasswordHash.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column user.sso_id
	 * @return  the value of user.sso_id
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getSsoId() {
		return ssoId;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column user.sso_id
	 * @param ssoId  the value for user.sso_id
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setSsoId(String ssoId) {
		this.ssoId = ssoId == null ? null : ssoId.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column user.sso_type
	 * @return  the value of user.sso_type
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getSsoType() {
		return ssoType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column user.sso_type
	 * @param ssoType  the value for user.sso_type
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setSsoType(String ssoType) {
		this.ssoType = ssoType == null ? null : ssoType.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column user.regist_date
	 * @return  the value of user.regist_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public Date getRegistDate() {
		return registDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column user.regist_date
	 * @param registDate  the value for user.regist_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setRegistDate(Date registDate) {
		this.registDate = registDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column user.regist_user
	 * @return  the value of user.regist_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getRegistUser() {
		return registUser;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column user.regist_user
	 * @param registUser  the value for user.regist_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setRegistUser(String registUser) {
		this.registUser = registUser == null ? null : registUser.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column user.update_date
	 * @return  the value of user.update_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column user.update_date
	 * @param updateDate  the value for user.update_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column user.update_user
	 * @return  the value of user.update_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getUpdateUser() {
		return updateUser;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column user.update_user
	 * @param updateUser  the value for user.update_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser == null ? null : updateUser.trim();
	}
}