package com.iosys.join.entity;

import java.util.Date;

public class Organization {

	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column organization.id
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private Short id;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column organization.name
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String name;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column organization.financial_institution_cd
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private Short financialInstitutionCd;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column organization.account_type
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private Byte accountType;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column organization.branch_code
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String branchCode;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column organization.bank_account
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String bankAccount;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column organization.regist_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private Date registDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column organization.regist_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String registUser;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column organization.update_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private Date updateDate;
	/**
	 * This field was generated by MyBatis Generator. This field corresponds to the database column organization.update_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	private String updateUser;

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column organization.id
	 * @return  the value of organization.id
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public Short getId() {
		return id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column organization.id
	 * @param id  the value for organization.id
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setId(Short id) {
		this.id = id;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column organization.name
	 * @return  the value of organization.name
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getName() {
		return name;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column organization.name
	 * @param name  the value for organization.name
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setName(String name) {
		this.name = name == null ? null : name.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column organization.financial_institution_cd
	 * @return  the value of organization.financial_institution_cd
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public Short getFinancialInstitutionCd() {
		return financialInstitutionCd;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column organization.financial_institution_cd
	 * @param financialInstitutionCd  the value for organization.financial_institution_cd
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setFinancialInstitutionCd(Short financialInstitutionCd) {
		this.financialInstitutionCd = financialInstitutionCd;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column organization.account_type
	 * @return  the value of organization.account_type
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public Byte getAccountType() {
		return accountType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column organization.account_type
	 * @param accountType  the value for organization.account_type
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setAccountType(Byte accountType) {
		this.accountType = accountType;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column organization.branch_code
	 * @return  the value of organization.branch_code
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getBranchCode() {
		return branchCode;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column organization.branch_code
	 * @param branchCode  the value for organization.branch_code
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode == null ? null : branchCode.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column organization.bank_account
	 * @return  the value of organization.bank_account
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getBankAccount() {
		return bankAccount;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column organization.bank_account
	 * @param bankAccount  the value for organization.bank_account
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount == null ? null : bankAccount.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column organization.regist_date
	 * @return  the value of organization.regist_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public Date getRegistDate() {
		return registDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column organization.regist_date
	 * @param registDate  the value for organization.regist_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setRegistDate(Date registDate) {
		this.registDate = registDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column organization.regist_user
	 * @return  the value of organization.regist_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getRegistUser() {
		return registUser;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column organization.regist_user
	 * @param registUser  the value for organization.regist_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setRegistUser(String registUser) {
		this.registUser = registUser == null ? null : registUser.trim();
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column organization.update_date
	 * @return  the value of organization.update_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public Date getUpdateDate() {
		return updateDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column organization.update_date
	 * @param updateDate  the value for organization.update_date
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

	/**
	 * This method was generated by MyBatis Generator. This method returns the value of the database column organization.update_user
	 * @return  the value of organization.update_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public String getUpdateUser() {
		return updateUser;
	}

	/**
	 * This method was generated by MyBatis Generator. This method sets the value of the database column organization.update_user
	 * @param updateUser  the value for organization.update_user
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	public void setUpdateUser(String updateUser) {
		this.updateUser = updateUser == null ? null : updateUser.trim();
	}
}