package com.iosys.join.map;

import java.util.List;
import org.apache.ibatis.annotations.Param;

import com.iosys.join.entity.PaymentMethodMaster;
import com.iosys.join.entity.PaymentMethodMasterExample;

public interface PaymentMethodMasterMapper {

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table payment_method_master
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	long countByExample(PaymentMethodMasterExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table payment_method_master
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	int deleteByExample(PaymentMethodMasterExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table payment_method_master
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	int deleteByPrimaryKey(Short paymentMethodCd);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table payment_method_master
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	int insert(PaymentMethodMaster record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table payment_method_master
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	int insertSelective(PaymentMethodMaster record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table payment_method_master
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	List<PaymentMethodMaster> selectByExample(PaymentMethodMasterExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table payment_method_master
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	PaymentMethodMaster selectByPrimaryKey(Short paymentMethodCd);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table payment_method_master
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	int updateByExampleSelective(@Param("record") PaymentMethodMaster record,
			@Param("example") PaymentMethodMasterExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table payment_method_master
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	int updateByExample(@Param("record") PaymentMethodMaster record,
			@Param("example") PaymentMethodMasterExample example);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table payment_method_master
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	int updateByPrimaryKeySelective(PaymentMethodMaster record);

	/**
	 * This method was generated by MyBatis Generator. This method corresponds to the database table payment_method_master
	 * @mbg.generated  Fri Mar 02 18:02:23 JST 2018
	 */
	int updateByPrimaryKey(PaymentMethodMaster record);
}