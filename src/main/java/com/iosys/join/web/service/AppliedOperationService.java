package com.iosys.join.web.service;

import com.iosys.join.web.common.JoinReturnObject;

public class AppliedOperationService {

	/**
	 * スケジュール応募
	 * 
	 * @param アカウントID
	 * @param スケジュールID
	 * @param 入札方法コード
	 * @param 入札チケット数
	 * 
	 * @param 返却オブジェクト
	 * 
	 * 1.アカウントIDとスケジュールIDと入札方法コードの入力チェック
	 * どれかもしくは、全てが未入力の場合はエラー　返却オブジェクトの終了コードと終了メッセージを設定して返却
	 * 全て入力されていたら次の処理へ
	 * 
	 * 2.入札方法コードがオークションの場合
	 * 入札チケット数の入力チェック
	 * 未入力の場合はエラー　返却オブジェクトの終了コードと終了メッセージを設定して返却
	 * 入力されている場合は次の処理へ
	 * 
	 * 3.応募が可能かチェック
	 * 応募が可能でなかったら処理結果を返却して終了
	 * 応募が可能だったら次の処理へ
	 * 
	 * 4.ユーザースケジュールの追加
	 * ユーザーIDとスケジュールIDを使用してユーザースケジュールにレコードを追加
	 * 返却オブジェクトに終了コードと終了メッセージを設定して返却
	 * 
	 * */
	public JoinReturnObject createGuestSchedule(String accountId,String scheduleId,int biddingCd,int ticketNum) {
		return null;
	}
	
	/**
	 * スケジュールキャンセル
	 * 
	 * @param アカウントID
	 * @param スケジュールID
	 * 
	 * @return 返却オブジェクト
	 * 
	 * 1.アカウントIDとスケジュールIDの入力チェック
	 * 未入力がある場合はエラー　返却オブジェクトの終了コードと終了メッセージを設定して返却
	 * 両方とも入力されているなら次の処理へ
	 * 
	 * 2.スケジュールの募集方法とスケジュールの開始時刻をチェック
	 * 募集方法と開始時刻に応じたチケットの払い戻し処理を実施
	 * 返却オブジェクトに終了コードと終了メッセージを返却
	 * 
	 * */
	public JoinReturnObject cancelSchedule(String accountId,String scheduleId) {
		return null;
	}
}
