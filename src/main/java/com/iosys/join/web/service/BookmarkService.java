package com.iosys.join.web.service;

import java.util.Objects;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.iosys.join.dao.BookmarkDao;
import com.iosys.join.dao.GuestDao;
import com.iosys.join.dao.HostDao;
import com.iosys.join.entity.Bookmark;
import com.iosys.join.entity.Guest;
import com.iosys.join.entity.Host;

@Service
@Transactional
public class BookmarkService {

	@Autowired
	private BookmarkDao bookmarkDao;
	@Autowired
	private GuestDao guestDao;
	@Autowired
	private HostDao hostDao;
	//テストコード
	private Logger logger = Logger.getLogger(new Object(){}.getClass().getName());
	/**
	 * ブックマーク登録処理
	 * @param guestId 登録処理を実行したユーザーのID
	 * @param hostId 登録対象のユーザーのID
	 * 
	 * @return ブックマーク登録の処理結果コード<br>
	 * 正常終了：0<br>
	 * ブックマーク登録済み：1<br>
	 * その他の異常終了：9
	 * */
	public int insertBookmark(String guestId,String hostId) {
		
		//テストコード
		logger.info("処理開始 : " + new Object(){}.getClass().getName());
		
		int ret = 0;
		try {
			Guest guest = guestDao.selectOne(guestId);
			Host host = hostDao.selectOne(hostId);
			
			//ゲストとホストが両方とも存在する場合はブックマーク登録処理を実行
			if(!(Objects.equals(guest,null)) && !(Objects.equals(host,null))) {
				//ブックマーク登録済みチェック
				Bookmark bookmark = bookmarkDao.selectOne(guestId, hostId);
				
				if(Objects.equals(bookmark, null)) {
					//ブックマークに登録されていなかった場合は登録処理を実行
					bookmarkDao.insert(guestId, hostId);
				}
				else {
					//ブックマーク登録済みだった場合は戻り値をブックマーク登録済みの値に設定
					ret = 1;
				}
			}else {
				//どちらか、もしくは両方とも存在しない場合は戻り値をその他の異常終了の値に設定
				ret = 9;
			}
		}catch(Exception e) {
			//ログにエラーを出力する処理
			logger.error("例外発生 : " + new Object(){}.getClass().getName());
			//戻り値をその他の異常終了の値に設定
			ret = 9;
		}
		finally{
			//テストコード
			logger.info("処理終了 : " + new Object(){}.getClass().getName());
			
			return ret;
		}
	}
}
