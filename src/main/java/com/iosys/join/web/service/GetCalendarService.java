package com.iosys.join.web.service;

import org.springframework.stereotype.Service;

import com.iosys.join.web.common.CalendarObject;

@Service
public class GetCalendarService {

	/**
	 * ゲストカレンダー取得（マッチID有）
	 * @param userId  カレンダーを取得するユーザーのjoinユーザーID
	 * @param matchId 詳細を取得するマッチのID
	 * 
	 * @return 年月日とマッチ情報を紐づけたカレンダー情報
	 * */
	public CalendarObject guest(String userId,Integer matchId) {
		
		/*
		 * 1.userIdでお気に入りホスト取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 2.matchIdから情報取得
		 * 該当マッチが実施される年月日と年月とマッチ詳細を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 3.2で取得した年月の最終日を取得 
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 4.2で取得した年月のuserIdが当選済みマッチとuserIdが実施済みマッチを取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 5.2で取得した年月のお気に入りホストのマッチ情報を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 6.返却オブジェクトに年月日とマッチ情報を紐づけて格納
		 */
		
		/*
		 * 7.年月日とマッチ情報を紐づけた情報を返却して終了
		 */
		return null;
	}
	
	/**
	 * ゲストカレンダー取得（マッチID無）
	 * @param userId  カレンダーを取得するユーザーのjoinユーザーID
	 * 
	 * @return 年月日とマッチ情報を紐づけたカレンダー情報
	 * */
	public CalendarObject guest(String userId) {
		
		/*
		 * 1.userIdでお気に入りホスト取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 2.当月の情報を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 3.2で取得した年月の最終日を取得 
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 4.2で取得した年月のuserIdが当選済みマッチとuserIdが実施済みマッチを取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 5.2で取得した年月のお気に入りホストのマッチ情報を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 6.返却オブジェクトに年月日とマッチ情報を紐づけて格納
		 */
		
		/*
		 * 7.年月日とマッチ情報を紐づけた情報を返却して終了
		 */
		return null;
	}
	
	/**
	 * ホストカレンダー取得（マッチID有）
	 * @param userId  マッチ情報を取得するホストのjoinユーザーID
	 * @param matchId 詳細を取得するマッチのID
	 * 
	 * @return 年月日とマッチ情報を紐づけたカレンダー情報
	 * 
	 * */
	public CalendarObject host(String userId,Integer matchId) {
		
		/*
		 * 1.matchIdから情報取得
		 * 該当マッチが実施される年月日と年月とマッチ詳細を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 2.1で取得した年月のuserIdが登録したマッチ情報を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 3.返却オブジェクトに年月日とマッチ情報を紐づけて格納
		 */
		
		/*
		 * 4.年月日とマッチ情報を紐づけた情報を返却して終了
		 */
		return null;
	}
	
	/**
	 * ホストカレンダー取得（マッチID無）
	 * @param userId  マッチ情報を取得するホストのjoinユーザーID
	 * 
	 * @return 年月日とマッチ情報を紐づけたカレンダー情報
	 * 
	 * */
	public CalendarObject host(String userId) {
		
		/*
		 * 1.当月の年月を取得 
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 2.1で取得した年月のuserIdが登録したマッチ情報を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 3.返却オブジェクトに年月日とマッチ情報を紐づけて格納
		 */
		
		/*
		 * 4.年月日とマッチ情報を紐づけた情報を返却して終了
		 */
		return null;
	}
	
	/**
	 * 団体管理者カレンダー取得（マッチID有）
	 * @param userId マッチ情報を取得するホストのjoinユーザーID
	 * @param matchId 詳細を取得するマッチのID
	 * 
	 * @return 年月日とマッチ情報を紐づけたカレンダー情報
	 * 
	 * */
	public CalendarObject manager(String userId,Integer matchId) {
		
		
		/*
		 * 1.matchIdから情報取得
		 * 該当マッチが実施される年月日と年月とマッチ詳細を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 2.1で取得した年月のuserIdが登録したマッチ情報を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 3.userIdが所属する団体のホスト情報を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 4.1で取得した年月に3で取得したホストが登録したマッチ情報を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 5.返却オブジェクトに年月日とマッチ情報を紐づけて格納
		 */
		
		/*
		 * 6.年月日とマッチ情報を紐づけた情報を返却して終了
		 */
		
		return null;
	}
	
	/**
	 * 団体管理者カレンダー取得（マッチID無）
	 * @param userId マッチ情報を取得するホストのjoinユーザーID
	 * 
	 * @return 年月日とマッチ情報を紐づけたカレンダー情報
	 * 
	 * */
	public CalendarObject manager(String userId) {
		
		
		/*
		 * 1.当月の年月を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 2.1で取得した年月のuserIdが登録したマッチ情報を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 3.userIdが所属する団体のホスト情報を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 4.1で取得した年月に3で取得したホストが登録したマッチ情報を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 5.返却オブジェクトに年月日とマッチ情報を紐づけて格納
		 */
		
		/*
		 * 6.年月日とマッチ情報を紐づけた情報を返却して終了
		 */
		
		return null;
	}
}
