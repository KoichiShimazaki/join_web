package com.iosys.join.web.service;

import org.springframework.stereotype.Service;

import com.iosys.join.web.common.MyPageObject;

@Service
public class MyPageService {

	/**
	 * マイページ表示情報取得（ゲスト）
	 * @param userId マイページを表示するユーザーのID
	 * 
	 * @return ゲストのマイページで表示する項目を返却
	 * */
	public MyPageObject guestInfo(String userId) {
		
		/*
		 * 1.userIdでアカウントID、表示名、デフォルトタイトル、保有チケット数、メールアドレス、落選当選通知設定、お気に入りマッチ登録通知設定を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 2.userIdでお気に入りホストを取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 3.userIdで直近の実施済みマッチIDと直近の実施前マッチIDを取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 4.1、2、3で取得した情報を返却オブジェクトに格納して返却
		 */
		
		return null;
	}
	
	/**
	 * マイページ表示情報取得（ホスト）
	 * @param userId マイページを表示するユーザーのID
	 * 
	 * @param ホストのマイページで表示する項目を返却
	 * */
	public MyPageObject hostInfo(String userId) {
		
		/*
		 * 1.userIdでアカウントID、表示名、デフォルトタイトル、メールアドレス、活動開始日、活動累計日数、獲得ブックマーク数を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 2.userIdで評価サマリー情報を取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 3.userIdで直近の実施前の公開済みマッチID
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 4.userIdで当月、先月、先々月の年月、マッチ実施時間、平均単価、獲得チケット数を取得
		 * 取得できた場合は次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 5.1、2、3、4で取得した情報を返却オブジェクトに格納して返却
		 */
		return null;
	}
}
