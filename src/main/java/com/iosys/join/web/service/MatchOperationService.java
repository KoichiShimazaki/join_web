package com.iosys.join.web.service;

import org.springframework.stereotype.Service;

import com.iosys.join.web.common.JoinReturnObject;

@Service
public class MatchOperationService {

	/**
	 * マッチ登録
	 * @param hostId ホストのアカウントID
	 * @param titleCd ゲームタイトルコード
	 * @param comment マッチのコメント
	 * @param scheduleName マッチの名前
	 * @param date マッチの実施年月日
	 * @param startTime マッチの開始時間
	 * @param endTime マッチの終了時間
	 * @param biddingMethodCd 入札方法コード
	 * @param recruitmentNumbers 募集人数
	 * @param fixedDate 募集結果が確定する日
	 * @param requestTickets 要求チケット数
	 * @param releaseFlag 公開フラグ
	 * 
	 * @return 返却オブジェクト
	 * 
	 * 1.入力された項目をデータベースへ登録する
	 * 返却オブジェクトに取得結果と終了コードと終了メッセージを設定して返却
	 * 
	 * */
	public JoinReturnObject createHostMatch(String hostId,
											int titleCd,
											String comment,
											String scheduleName,
											String date,
											String startTime,
											String endTime,
											int biddingMethodCd,
											int recruitmentNumbers,
											String fixedDate,
											int requestTickets,
											boolean releaseFlag){
		return null;
	}
	
	/**
	 * マッチ更新
	 * @param hostId ホストのアカウントID
	 * @param titleCd ゲームタイトルコード
	 * @param comment マッチのコメント
	 * @param scheduleName マッチの名前
	 * @param date マッチの実施年月日
	 * @param startTime マッチの開始時間
	 * @param endTime マッチの終了時間
	 * @param biddingMethodCd 入札方法コード
	 * @param recruitmentNumbers 募集人数
	 * @param fixedDate 募集結果が確定する日
	 * @param requestTickets 要求チケット数
	 * @param releaseFlag 公開フラグ
	 * 
	 * @return 返却オブジェクト
	 * 
	 * 1.入力された項目でデータベースを更新する
	 * 返却オブジェクトに取得結果と終了コードと終了メッセージを設定して返却
	 * 
	 * */
	public JoinReturnObject  updateHostMatch(String hostId,
											int titleCd,
											String comment,
											String scheduleName,
											String date,
											String startTime,
											String endTime,
											int biddingMethodCd,
											int recruitmentNumbers,
											String fixedDate,
											int requestTickets,
											boolean releaseFlag){
		return null;
	}
	
	/**
	 * マッチ削除
	 * @param scheduleId 削除対象のマッチID
	 * 
	 * @return 返却オブジェクト
	 * 
	 * 1.マッチの削除
	 * マッチIDを使用してゲストマッチを削除
	 * 返却オブジェクトに終了コードと終了メッセージを設定して返却
	 * 
	 * */
	public JoinReturnObject deleteHostMatch(String scheduleId){
		return null;
	}
}
