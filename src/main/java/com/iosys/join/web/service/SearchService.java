package com.iosys.join.web.service;

import java.sql.Date;

import org.springframework.stereotype.Service;

import com.iosys.join.web.common.JoinReturnObject;

@Service
public class SearchService {

	/**
	 * ホスト取得
	 * @param titleCode ゲームタイトルコード
	 * @param hostName ホスト名
	 * @param date マッチの開始年月日
	 * @param searchStartPointTime 開始時間の始点
	 * @param serchEndPointTime 開始時間の終点
	 * @param serchResultStartPoint 検索結果の何番目を表示の始点にするかの設定
	 * @param serchResultEndPoint 検索結果の何番目を表示の終点にするかの設定
	 * 
	 * @return 返却オブジェクト
	 * 
	 * 1.ホストを入力された条件で検索
	 * 返却オブジェクトに取得結果と終了コードと終了メッセージを設定して返却
	 * 
	 * */
	public JoinReturnObject searchHosts(int titleCode,
								String hostName,
								String date,
								String searchStartPointTime,
								String serchEndPointTime,
								int serchResultStartPoint,
								int serchResultEndPoint) {
		return null;
	}
	
	/**
	 * マッチ取得
	 * @param titleCode ゲームタイトルコード
	 * @param hostName ホスト名
	 * @param date マッチの開始年月日
	 * @param searchStartPointTime 開始時間の始点
	 * @param serchEndPointTime 開始時間の終点
	 * @param serchResultStartPoint 検索結果の何番目を表示の始点にするかの設定
	 * @param serchResultEndPoint 検索結果の何番目を表示の終点にするかの設定
	 * 
	 * @return 返却オブジェクト
	 * 
	 * 1.マッチを入力された条件で検索
	 * 返却オブジェクトに取得結果と終了コードと終了メッセージを設定して返却
	 * 
	 * */
	public JoinReturnObject searchMatch(int titleCode,
										String hostName,
										String date,
										String searchStartPointTime,
										String serchEndPointTime,
										int serchResultStartPoint,
										int serchResultEndPoint) {
		return null;
	}
	
	/**
	 * 新着ホスト取得
	 * 
	 * @return 返却オブジェクト
	 * 
	 * 1.新しくマッチを登録した順にホストを取得
	 * 返却オブジェクトに取得結果と終了コードと終了メッセージを設定して返却
	 * 
	 * */
	public JoinReturnObject getNewHosts(){
		return null;
	}
	
	/**
	 * インフォメーション取得
	 * @param orgId 団体ID
	 * 
	 * @return 返却オブジェクト
	 * 
	 * 1.インフォメーションを新しい順にいくつか取得（取得数は未定）
	 * 団体IDは指定されているときのみ、検索条件に加える
	 * 返却オブジェクトに取得結果と終了コードと終了メッセージを設定して返却
	 * 
	 * */
	public JoinReturnObject getInformation(String orgId){
		return null;
	}
	
	/**
	 * マイマッチ取得
	 * @param userId ユーザーID
	 * @param targetDate 実行された年月日
	 * @return 
	 * 
	 * @return 返却オブジェクト
	 * 
	 * 1.userIdとdateをキーにマッチを検索
	 * 返却オブジェクトに取得結果と終了コードと終了メッセージを設定して返却
	 * 
	 * */
	public JoinReturnObject getMyMatch(String userId, Date targetDate){
		return null;
	}
	
	/**
	 * ユーザー情報取得
	 * @param userId ユーザーID
	 * @return 
	 * 
	 * @return 返却オブジェクト
	 * 
	 * 1.userIdをキーにユーザー情報を検索
	 * 返却オブジェクトに取得結果と終了コードと終了メッセージを設定して返却
	 * 
	 * */
	public JoinReturnObject getMyData(String userId){
		return null;
	}
	
	/**
	 * 活動状況取得
	 * @param userId ユーザーID
	 * 
	 * @return 返却オブジェクト
	 * 
	 * 1.userIdをキーに活動状況を取得
	 * 返却オブジェクトに取得結果と終了コードと終了メッセージを設定して返却
	 * 
	 * */
	public JoinReturnObject getActivities(String userId){
		return null;
	}
	
	/**
	 * 団体取得チケット数取得
	 * 
	 * @param orgId 団体ID
	 * 
	 * @return 返却オブジェクト
	 * 
	 * 1.orgIdをキーに団体に所属しているユーザーのチケット数の合計を取得
	 * 返却オブジェクトに取得結果と終了コードと終了メッセージを設定して返却
	 * 
	 * */
	public JoinReturnObject getOrgTicketSum(String orgId){
		return null;
	}
	
	/**
	 * 団体所属ユーザー取得
	 * 
	 * @param orgId 団体ID
	 * @return 
	 * 
	 * @return 返却オブジェクト
	 * 
	 * 1.orgIdをキーに団体に所属しているユーザー情報を取得
	 * 返却オブジェクトに取得結果と終了コードと終了メッセージを設定して返却
	 * 
	 * */
	public JoinReturnObject getBelongAccount(String orgId){
		return null;
	}
	
	/**
	 * お気に入り取得
	 * 
	 * @param userId ユーザーID
	 * 
	 * @return 返却オブジェクト
	 * 
	 * 1.userIdをキーにお気に入り情報を取得
	 * 返却オブジェクトに取得結果と終了コードと終了メッセージを設定して返却
	 * 
	 * */
	public JoinReturnObject getMyFavorite(String userId){
		return null;
	}
	
	/**
	 * マッチ詳細取得
	 * 
	 * @param matchId マッチID
	 * 
	 * @param 返却オブジェクト
	 * 
	 * 1.userIdをキーにマッチ詳細を取得
	 * 返却オブジェクトに取得結果と終了コードと終了メッセージと設定して返却
	 * 
	 * */
	public JoinReturnObject getMatchDetail(String matchId){
		return null;
	}
}
