package com.iosys.join.web.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * マイページ（ゲスト）画面用コントローラー
 * @author shimazaki
 *
 */
@Controller
@RequestMapping("g/mypage")
public class GuestMyPageController {

	@Autowired
	HttpSession session;
	
	/**
	 * マイページ画面表示
	 * @param model thymeleafのテンプレートに値を渡す用
	 * 
	 * @return      ゲストマイページ画面を表示
	 * 
	 * */
	@GetMapping
	public ModelAndView index(ModelAndView model) {
		
		/* 1.sessionからユーザーIDを取得
		 * 取得できたら次の処理へ
		 * 取得できなかった場合はエラー
		 */
		
		/*
		 * 2.ゲスト共通機能でゲスト画面共通表示項目を取得
		 * 正常終了の場合は次の処理へ
		 * 異常終了した場合はエラー
		 */
		
		/*
		 * 3.アカウント情報を取得
		 * 正常終了した場合は次の処理へ
		 * 異常終了した場合はエラー 
		 */
		
		/*
		 * 4.表示項目の返却
		 * 表示項目を設定しゲストカレンダー画面のテンプレートを指定して返却
		 */
		
		return model;
	}
	
	/**
	 * アカウント情報更新
	 * @param userName      表示名
	 * @param titleCd       デフォルトタイトルコード
	 * @param mailAddress   登録メールアドレス
	 * @param winning       応募マッチの当選通知
	 * @param matchRegister お気に入りホストのマッチ登録通知
	 * @param password      変更後のパスワード
	 * @param delFavorite   お気に入りホスト削除情報
	 * 
	 * @return              ゲストのマイページを表示
	 * 
	 * */
	@PostMapping
	public String update(@RequestParam(name="userName") String userName,
			@RequestParam(name="titleCd") int titleCd,
			@RequestParam(name="mailAddress") String mailAddress,
			@RequestParam(name="winning") int winning,
			@RequestParam(name="matchRegister") int matchRegister,
			@RequestParam(name="password") String password,
			@RequestParam(name="delFavorite") Object delFavorite) {//お気に入りホスト削除情報は仮置き　どんな型でもらうか要検討
		
		/*
		 * 1.sessionからユーザーIDを取得
		 * 取得できたら次の処理へ
		 * 取得できなかったらエラー
		 */
		
		/*
		 * 2.1で取得したユーザーIDと引数項目を使用してDBを更新
		 * 更新できたら次の処理へ
		 * 更新できなかったらエラー
		 */
		
		/*
		 * 3.マイページへ遷移
		 */
		return "redirect:index";
	}
}