package com.iosys.join.web.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.iosys.join.web.service.SearchService;

/**
 * 検索画面用コントローラー
 * @author shimazaki
 *
 */
@Controller
@RequestMapping("search")
public class SearchController {

	@Autowired
	HttpSession session;
	
	@Autowired
	SearchService service;
	
	/**
	 * 初期表示
	 * @return
	 */
	public void index(Model model,HttpServletResponse res) {
	}
	
	/**
	 * 検索画面表示
	 * @param thymeleafのテンプレートに値を渡す用
	 * @param res   cookie取得用
	 * @return 検索画面を表示
	 * */
	public String searchResult(Model model,HttpServletResponse res) {
		
		/*
		 * 1.ゲスト共通機能でゲスト画面共通表示項目を取得
		 * 正常終了の場合は次の処理へ
		 * 異常終了した場合はエラー
		 */
		/*
		 * 2.表示項目の返却
		 * 未ログインorゲスト用の表示項目を設定し検索画面のテンプレートを指定して返却
		 */	
		return "search";
	}	
}
