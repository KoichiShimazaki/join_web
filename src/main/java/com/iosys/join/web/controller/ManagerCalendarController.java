package com.iosys.join.web.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * カレンダー（団体理管）画面用コントローラー
 * @author shimazaki
 *
 */
@Controller
@RequestMapping("m/calendar")
public class ManagerCalendarController {

	@Autowired
	HttpSession session;
	
	/**
	 * カレンダー（団体管理）画面表示
	 * @param model   thymeleafのテンプレートに値を渡す用
	 * @param res     cookie取得用
	 * @param matchId 未入力可の項目<br>
	 *                入力されていたら該当マッチの実施される年月のカレンダーと主催ホストのマッチ実施年月日のマッチ情報とマッチ詳細を取得する
	 * 
	 * @return        団体管理者カレンダー画面を表示
	 * */
	@GetMapping
	public String index(Model model,
			HttpServletResponse res,
			@RequestParam(name="matchId",required=false) String matchId) {
		/*
		 * 1.ホスト共通機能でホスト画面共通表示項目を取得
		 * 正常終了の場合は次の処理へ
		 * 異常終了した場合はエラー
		 */
		/*
		 * 2.matchIdの入力チェック
		 * 入力されていた場合は全ての文字が数字であるかを確認
		 * 全ての文字が数字の場合は次の処理へ
		 * 数字以外の文字が含まれている場合はエラー
		 */
		/*
		 * 3.表示項目の取得
		 * matchIdが未入力の場合は、当月分のカレンダー情報を取得
		 * matchIdが入力されている場合は、該当するマッチが実施される年月のカレンダー情報、を取得
		 */
		/*
		 * 4.表示項目の返却
		 * 表示項目を設定しゲストカレンダー画面のテンプレートを指定して返却
		 */
		return "managerclendar";
	}
}
