package com.iosys.join.web.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * ヘルプ画面用コントローラー
 * @author shimazaki
 *
 */
@RequestMapping("thanks")
public class HelpController {

	/**
	 * 問い合わせメール送信
	 * @param mailaddress 問い合わせフォームに入力されたメールアドレス　問い合わせの返答に使用
	 * @param userId      JOINのユーザーID
	 * @param inquiry     問い合わせ内容
	 * 
	 * @return            問い合わせメール送信処理の終了コード
	 * */
	@PostMapping
	public ModelAndView sendInquiryMail(ModelAndView model,
								@RequestParam("mailaddress") String mailaddress,
								@RequestParam(name="userId" ,required=false) String userId,
								@RequestParam("inquiry") String inquiry) {
		
		/*
		 * 1.問い合わせ受付メールアドレスに問い合わせ内容を送信
		 * 正常終了した場合は次の処理へ
		 * 異常終了した場合はエラー
		 */
		
		/*
		 * 2.表示項目の返却
		 * 表示項目に問い合わせ内容を設定し問い合わせ受付画面テンプレートを指定して返却
		 */
		
		return model;
	}
}
