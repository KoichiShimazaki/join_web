package com.iosys.join.web.controller;

import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import com.iosys.join.csv.OrgReportCsv;

/**
 * 団体管理画面用コントローラー
 * @author shimazaki
 *
 */
@Controller
//@RequestMapping("/orgmanagement")
//@EnableAutoConfiguration
public class OrganizationManagementController {
	
	@Autowired
	HttpSession session;
	
	/**
	 * 団体管理画面表示
	 * @param model thymeleafのテンプレートに値を渡す用
	 * @param res   cookie取得用
	 * 
	 * @return      団体管理画面を表示
	 * */
	@RequestMapping("aa")
	public String index(Model model) {
		System.out.println("asdasda");
		/*
		 * 1.ホスト共通機能でホスト画面共通表示項目を取得
		 * 正常終了の場合は次の処理へ
		 * 異常終了した場合はエラー
		 */
		System.out.println("bbbbb");
		/*
		 * 2.表示項目の取得
		 * 正常終了の場合は次の処理へ
		 * 異常終了した場合はエラー
		 */
		/*
		 * 3.表示項目の返却
		 * 表示項目を設定し団体管理画面のテンプレートを指定して返却
		 */
		model.addAttribute("message", "Hello Thymeleaf!!");
		return "orgmanagement";
	}

	/**
	 * 団体管理情報更新
	 * @param model thymeleafのテンプレートに値を渡す用
	 * @param res   cookie取得用
	 * 
	 * @return      団体管理画面を表示
	 * */
	@PostMapping("update")
	public String update(Model model,HttpServletResponse res) {
		/*
		 * 1.ホスト共通機能でホスト画面共通表示項目を取得
		 * 正常終了の場合は次の処理へ
		 * 異常終了した場合はエラー
		 */
		/*
		 * 2.表示項目の取得
		 * 正常終了の場合は次の処理へ
		 * 異常終了した場合はエラー
		 */
		/*
		 * 3.表示項目の返却
		 * 表示項目を設定し団体管理画面のテンプレートを指定して返却
		 */
		return "orgmanagement";
	}


	/**
	 * 団体管理情報更新
	 * @param model thymeleafのテンプレートに値を渡す用
	 * @param res   cookie取得用
	 * 
	 * @return      団体管理画面を表示
	 * */
	@PostMapping("cancel")
	public String cancel(Model model,HttpServletResponse res) {
		/*
		 * 1.ホスト共通機能でホスト画面共通表示項目を取得
		 * 正常終了の場合は次の処理へ
		 * 異常終了した場合はエラー
		 */
		/*
		 * 2.表示項目の取得
		 * 正常終了の場合は次の処理へ
		 * 異常終了した場合はエラー
		 */
		/*
		 * 3.表示項目の返却
		 * 表示項目を設定し団体管理画面のテンプレートを指定して返却
		 */
		return "orgmanagement";
	}
	
	/**
	 * レポート情報CSV取得処理
	 * 
	 * @param orgId  団体ID
	 * @param yyyymm 年月
	 * 
	 * CSVで所属ホストのマッチ履歴を取得する
	 */
	@GetMapping(value = "aa.csv", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE
			+ "; charset=Shift_JIS; Content-Disposition: attachment")
	@ResponseBody
	public Object getCsvReport() throws JsonProcessingException {
		
		//団体IDと年月から完了したマッチの申し込み情報を取得
		//TODO
		
		//http://localhost:8080/orgmanagement/aa.csv
		//出力用に加工
		ArrayList<OrgReportCsv> demos = new ArrayList<OrgReportCsv>() {
			{
				add(new OrgReportCsv(1001L, "aaa", "xxx", new Date()));
				add(new OrgReportCsv(1002L, "bbb", "yyy", new Date()));
				add(new OrgReportCsv(1003L, "ccc", "zzz", new Date()));
			}
		};

		CsvMapper mapper = new CsvMapper();
		CsvSchema schema = mapper.schemaFor(OrgReportCsv.class).withHeader();
		return mapper.writer(schema).writeValueAsString(demos);
	}
}
