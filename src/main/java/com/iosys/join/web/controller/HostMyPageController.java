package com.iosys.join.web.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * マイページ（ホスト）画面用コントローラー
 * @author shimazaki
 *
 */
@Controller
@RequestMapping("h/mypage")
public class HostMyPageController {
	
	@Autowired
	HttpSession session;
	
	/**
	 * マイページ（ホスト）画面表示
	 * @param model thymeleafのテンプレートに値を渡す用
	 * @param res cookie取得用
	 * 
	 * @return ホストマイページ画面を表示
	 * 
	 * */
	@GetMapping
	public String index(Model model,HttpServletResponse res) {
		/*
		 * 1.ホスト共通機能でホスト画面共通表示項目を取得
		 * 正常終了の場合は次の処理へ
		 * 異常終了した場合はエラー
		 */
		
		/*
		 * 2.アカウント情報を取得
		 * 正常終了した場合は次の処理へ
		 * 異常終了した場合はエラー 
		 */
		
		/*
		 * 3.表示項目の返却
		 * 表示項目を設定しホストカレンダー画面のテンプレートを指定して返却
		 */
		
		return "hostmypage";//テンプレート名は仮置き
	}
	

	/**
	 * ユーザー情報更新
	 * @param model
	 * @param res
	 * @return
	 */
	@PostMapping
	public String update(Model model,HttpServletResponse res) {
		return null;
	}
	
}
