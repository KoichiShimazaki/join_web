package com.iosys.join.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.iosys.join.dao.BookmarkDao;
import com.iosys.join.dao.BookmarkDaoImpl;
import com.iosys.join.dao.GuestDao;
import com.iosys.join.dao.GuestDaoImpl;
import com.iosys.join.dao.HostDao;
import com.iosys.join.dao.HostDaoImpl;

@Configuration
public class AppConfig {

	@Bean
	HostDao hostDao() {
		return new HostDaoImpl();
	}
	@Bean
	GuestDao guestDao() {
		return new GuestDaoImpl();
	}
	
	@Bean
	BookmarkDao bookmarkDao() {
		return new BookmarkDaoImpl();
	}
}
